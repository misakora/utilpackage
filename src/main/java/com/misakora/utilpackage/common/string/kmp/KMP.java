package com.misakora.utilpackage.common.string.kmp;

import java.util.ArrayList;
import java.util.List;

/**
 * KMP匹配模式串
 * @author misakora
 */
public class KMP {




    /**
     * 求KMP算法的临时数组next[]
     * @param pattern 模式串
     * @return next
     */
    public static int[] computeTemporaryArray(char pattern[]){
        int[] next = new int[pattern.length];
        int j = 0;
        for (int i = 1; i < next.length;) {
            if (pattern[i] == pattern[j]){
                next[i] = j+1;
                i++;
                j++;
            }else {
                if(j != 0){
                    j = next[j-1];
                }else {
                    next[i] = 0;
                    i++;
                }
            }
        }
        return next;
    }

    /**
     *
     * KMP算法本体
     * @param text 主串
     * @param pattern 模式串
     * @return boolean
     */
    public static boolean KMPReturnBoolean(String text , String pattern){
        return KMPReturnBoolean(text.toCharArray(),pattern.toCharArray());
    }
    public static boolean KMPReturnBoolean(char[] text , char[] pattern){
        int[] next = computeTemporaryArray(pattern);
        int i=0,j=0;
        while(i < text.length && j<pattern.length){
            if (text[i] == pattern[j]){
                i++;
                j++;
            }else{
                if (j==0){
                    i++;
                }else {
                    j= next[j-1];
                }
            }
        }
        if (j == pattern.length){
            return true;
        }
        return false;
    }


    /**
     * KMP算法本体 返回匹配到的数量
     * @param text 主串
     * @param pattern 模式串
     * @return int 匹配到的字符串的数量
     */
    public static int KMPReturnCount(String text , String pattern){
        return KMPReturnCount(text.toCharArray(),pattern.toCharArray());
    }
    public static int KMPReturnCount(char[] text , char[] pattern){
        int[] next = computeTemporaryArray(pattern);
        int i=0;
        int j=0;
        int count=0;
        while(i < text.length && j<pattern.length){
            if (text[i] == pattern[j]){
                i++;
                j++;
            }else{
                if (j==0){
                    i++;
                }else {
                    j= next[j-1];
                }
            }
            if (j >= pattern.length){
                j = 0;
                i++;
                count++;
            }
        }
       return count;
    }


    /**
     * KMP算法本体 返回匹配到的所有下标
     * @param text 主串
     * @param pattern 模式串
     * @return List 匹配到的字符串的下标
     */
    public static List KMPReturnIndex(String text , String pattern){
        return KMPReturnIndex(text.toCharArray(),pattern.toCharArray());
    }
    public static List KMPReturnIndex(char[] text , char[] pattern){
        int[] next = computeTemporaryArray(pattern);
        int i=0;
        int j=0;
        int index = -1;
        List<Integer> indexList = new ArrayList<Integer>();
        while(i < text.length && j<pattern.length){
            if (text[i] == pattern[j]){
                if (index == -1){
                    index = i;
                }
                i++;
                j++;
            }else{
                if (j==0){
                    i++;
                    index = -1;
                }else {
                    j= next[j-1];
                    index = i - next[j-1];
                }
            }
            if (j >= pattern.length){
                j = 0;
                i++;
                indexList.add(index);
                index = -1;
            }
        }
        return indexList;
    }

}
