package com.misakora.utilpackage.common.string;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class StringUtil {

    /**
     * 拼接字符串数组
     * @param arr 数组
     * @param reg 拼接用的字符串
     * @return String
     */
    public static String join(int[] arr ,String reg){
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str += arr[i];
            if (i == arr.length-1) continue;
            str += reg;
        }
        return str;
    }

    /**
     * 拼接字符串数组
     * @param obj 字符串数组
     * @param reg 拼接用的字符串
     * @return String
     */
    public static String join(Object[] obj ,String reg){
        String str = "";
        if (obj instanceof String[]){
            for (int i = 0; i < obj.length; i++) {
                str += obj[i];
                if (i == obj.length-1) continue;
                str += reg;
            }
            return str;
        }
        if (obj instanceof Integer[]){

        }
        return null;
    }

    /**
     * 拼接文件名
     * @param fileName
     * @return
     */
    public static  String subFileType(String fileName){
        return fileName.substring(fileName.lastIndexOf(".")+1);
    }

    /**
     * 创建新的文件名,通过日期
     * @return
     */
    public static String createNewNameByDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return sdf.format(new Date());
    }

    /**
     *
     * @return
     */
    public static String createNewNameByUUID(){
        UUID uid = UUID.randomUUID();
        return uid.toString().replace("-", "");
    }

}
