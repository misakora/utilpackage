package com.misakora.utilpackage.common.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateUtil {
	
	 /**
	  * 日期按格式转换成字符串
	  * @param date 日期
	  * @param p ָ 格式字符串
	  ** @return String
	  */
	 public static String dateToString(Date date,String p){
		   SimpleDateFormat sdf = new SimpleDateFormat(p);
		   return sdf.format(date);
	 }
	 
	 /**
	  * 把当前日期按格式转换成字符串
	  * @param p 格式字符串
	  * @return String
	  */
	 public static String currentDateToString(String p){
		   SimpleDateFormat sdf = new SimpleDateFormat(p);
		   return sdf.format(new Date());
	 }
	 
	 /**
	  * 字符串按格式转成日期
	  * @param datestr 日期字符串
	  * @param p 格式字符串
	  * @return Date
	  * @throws ParseException
	  */
	 public static Date stringToDate(String datestr,String p) throws ParseException{
		   SimpleDateFormat sdf = new SimpleDateFormat(p);
		   return sdf.parse(datestr);
	 }

	 
}
