package com.misakora.utilpackage.datastruct.tree;

public class BSTree {

    Node root;

    public BSTree(){

    }

    /**
     * 获取平衡因子
     * @param node
     * @return
     */
    public int getBalance(Node node){
        if (node == null){
            return 0;
        }
        return getBalance(node.left) - getBalance(node.right);
    }

    /**
     * 获取高度
     * @param node
     * @return
     */
    public int getHeight(Node node){
        if (node == null){
            return 0;
        }
        return node.height;
    }





}
