package com.misakora.utilpackage.datastruct.tree;

public class Node <T> {

    T val;
    Node left;
    Node right;
    int height;
    int balance;

    public Node() {
    }

    public Node(T val) {
        this.val = val;
        this.height = 1;
        this.balance = 0;
    }

    @Override
    public String toString() {
        return "Node{" +
                "val=" + val +
                ", left=" + left +
                ", right=" + right +
                ", height=" + height +
                ", balance=" + balance +
                '}';
    }

    public T getVal() {
        return val;
    }

    public void setVal(T val) {
        this.val = val;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
