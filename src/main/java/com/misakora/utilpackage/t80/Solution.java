package com.misakora.utilpackage.t80;

import java.util.HashMap;
import java.util.Map;

public class Solution {

    public Solution() {
    }

/*    public int removeDuplicates(int[] nums) {
        if (nums == null && nums.length==0){
            return 0;
        }
        int count = 1;
        int len = nums.length;
        for (int i = 1; i < len;) {
            if (nums[i] == nums[i-1]){
                count++;
                if (count>2){
                    //前移
                    for (int j = i; j < len-1; j++) {
                        nums[j] = nums[j+1];
                    }
                    len--;
                }else {
                    i++;
                }
            }else {
                count = 1;
                i++;
            }
        }
        return len;
    }*/

    public int removeDuplicates(int[] nums) {
        if (nums == null && nums.length==0){
            return 0;
        }
        if (nums.length<=2){
            return 2;
        }
        int len = nums.length;
        int i = 2,j = 2;
        while (j<len){
            if (nums[i-2] != nums[j]){
                nums[i] = nums[j];
                i++;
            }
            j++;
        }
        return i;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums = {1,1,1,2,2,3};
        int len = solution.removeDuplicates(nums);
        for (int n:
        nums){
            System.out.println(n);
        }
    }

}
