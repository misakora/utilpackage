package com.misakora.designpatterns.strategy;

import com.misakora.utilpackage.datastruct.sort.SortUtil;

import java.util.Comparator;

public class Sorter<T> {

    public Sorter() {
    }

    public void sort(int[] arr){
        SortUtil sortUtil = new SortUtil();
        sortUtil.quickSort(arr);
    }
    public void sort(Comparable[] arr){
        SortUtil sortUtil = new SortUtil();
        sortUtil.quickSort(arr);
    }

    public void sort(T[] catArr, AnimalComparator<T> catAnimalComparator) {
        SortUtil<T> sortUtil = new SortUtil();
        sortUtil.quickSort(catArr,catAnimalComparator);
    }
}
