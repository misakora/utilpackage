package com.misakora.designpatterns.strategy;

public interface AminalComparable<T> {

      int compareTo(T o);
}
