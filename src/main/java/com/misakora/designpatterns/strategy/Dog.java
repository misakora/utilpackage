package com.misakora.designpatterns.strategy;

public class Dog{

    public int food;

    public Dog() {
    }

    public Dog(int food) {
        this.food = food;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "food=" + food +
                '}';
    }
}
