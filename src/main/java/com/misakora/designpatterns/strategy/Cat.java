package com.misakora.designpatterns.strategy;

import java.util.Comparator;

public class Cat implements AminalComparable<Cat>{

    private String name;
    public double weight;
    public double height;

    public Cat() {
    }

    public Cat(String name ,double height, double weight) {
        this.name = name;
        this.height = height;
        this.weight = weight;
    }



    /*@Override
    public int compare(Cat o1, Cat o2) {
        if (o1.weight>o2.weight) return 1;
        else if (o1.weight == o2.weight) return 0;
        else return -1;
    }*/

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                '}';
    }


    public int compareTo(Cat o) {
        if (this.weight>o.weight) return 1;
        else if (this.weight == o.weight) return 0;
        else return -1;
    }
}
