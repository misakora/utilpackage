package com.misakora.designpatterns.strategy;

import java.util.Arrays;

public class Main {

    public Main() {
    }

    public static void main(String[] args) {
        int[] arr = {9,6,4,8,3,7,2,5,1,10};
        Cat[] catArr = {
                new Cat("tom",10,3),
                new Cat("mary",5,5),
                new Cat("jack",13,1),
        };
        Dog[] dogArr = {
                new Dog(3),
                new Dog(5),
                new Dog(1),
        };
        Sorter<Cat> sorter = new Sorter();
        sorter.sort(catArr, new CatWeightComparator());
        System.out.println("Weight: -- "+Arrays.toString(catArr));
        sorter.sort(catArr, new CatHeightComparator());
        System.out.println("Height: -- "+Arrays.toString(catArr));

        Sorter<Dog> sorter2 = new Sorter();
        sorter2.sort(dogArr, new DogFoodComparator());
        System.out.println(Arrays.toString(dogArr));
    }

}
