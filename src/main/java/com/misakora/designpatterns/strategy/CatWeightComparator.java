package com.misakora.designpatterns.strategy;

public class CatWeightComparator implements AnimalComparator<Cat>{

    public CatWeightComparator() {
    }

    @Override
    public int compare(Cat o, Cat o1) {
        if (o.weight>o1.weight) return 1;
        else if (o.weight == o1.weight) return 0;
        else return -1;
    }

}
