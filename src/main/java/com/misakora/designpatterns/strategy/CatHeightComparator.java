package com.misakora.designpatterns.strategy;

public class CatHeightComparator implements AnimalComparator<Cat>{

    @Override
    public int compare(Cat o, Cat o1) {
        if (o.height>o1.height) return 1;
        else if (o.height == o1.height) return 0;
        else return -1;
    }

}
