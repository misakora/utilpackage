package com.misakora.designpatterns.strategy;

public class DogFoodComparator implements AnimalComparator<Dog>{

    public DogFoodComparator() {
    }

    @Override
    public int compare(Dog o, Dog o1) {
        if (o.food>o1.food) return 1;
        else if (o.food == o1.food) return 0;
        else return -1;
    }

}
