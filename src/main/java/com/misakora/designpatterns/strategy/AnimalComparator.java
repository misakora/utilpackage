package com.misakora.designpatterns.strategy;

import java.util.Comparator;

public interface AnimalComparator<T>  {

    public int compare(T o1, T o2) ;
}
